## 2015 UMKC IoT Hackathon - SMART Home

This is the work of UMKC students as part of 2015 UMKC Hackathon sponsored by UMKC and various local and national organizations including IBM, DST, AdKnowledge, Ericsson and Cerner.
Our team's objective for the Hackathon was to use IBM IoT platform to create a SMART (Self-Monitoring Automated Residential Technology) solution. For our use case, we monitored temperature sensor data from a Texas Instrument Sensor Tag device. We hosted a NodeRED server on a Raspberry Pi device and stored data as it was being streamed from the TI Sensor Tag. Data was stored in JSON format to be consumed by services that would be implemented later.  

## Code Example
The JSON stored appeared as follows:
[{
    "json_data": {
        "object": 20.0625,
        "ambient": 23.75,
        "inHome": "home",
        "season": "winter",
        "timestamp": 1447320569993
    }
},
{
    "json_data": {
        "object": 19.84375,
        "ambient": 23.75,
        "inHome": "home",
        "season": "winter",
        "timestamp": 1447320570999
    }
}]

We stored timestamps so that we could plot temperature over time in a real-time line graph. After we would demonstrate success using this data set, our plan was to potentially include data from other sensors as well. As time constraints narrowed, we decided to focus on temperature only. The season was tracked in case a user would want to have different temperatures of their home during different times of the year. The "inHome" value could be "home," "away," or "nearby." This way, if a user was nearby, for example, the furnace would receive a signal to turn itself on and start raising the temperature. "Nearby" would be toggled if the system knew the owner was returning home soon - our assumption is that the system would know when the owner would be returning from being "away."

## Motivation

The reason we implemented our use case is so that home owners could have a way to save money on electricity by having a simple system established and connected to their mobile phone. The owner could program times for when they plan to be away and, in the winter, the system will turn the furnace off a short time before the owner leaves. Later, when the owner is returning home soon, the furnace will turn on so the home will return to a comfortable temperature by the time the owner returns. The owner will be comfortable with the temperature of their home and they will also save money.

## Visualization

We have used IBM Data Gateway kit for visualization. For instance, when the owner is away from home and the heat sensor detects a temprature above preset threshold, an alert visualization is triggered that will display blinking red light until the temprature goes down.

## Limitations - FrontEnd and Technical

We wanted to develop a front-end with some services that could dynamically work with the TI Sensor Tag data. We were able to create post requests to update an owner's status that could be retrieved, but we had time constraints with fully completing a use case of an owner turning off their furnace, for example. Our UI controls include controls for an owner to update the current seasonal configuration for his/her temperatures. The owner is also able to signal whether he is at home, nearby, or away. If the owner would be nearby, the SMART home system would be able to recognize that signal and turn on the furnace so the home is warm when the owner would return.

Technical limitations were that we were unable to send data to Bluemix. The integration with the data gateway services made it difficult for us to interact with the time series database. On the contrary, the Raspberry Pi hosted a hardware server for us to store information. It did not have sufficient services to connect to a Twilio node for sending SMS updates to a homeowner.

## Contributors

@jdlars, @hastimal, @thismali, @sarathchandrab



