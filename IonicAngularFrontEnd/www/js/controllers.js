angular.module('starter.controllers', [])

  .config(function ($httpProvider) {
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

  })
  .controller('AppCtrl', function($scope, $ionicModal, $timeout) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
      $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
      $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
      console.log('Doing login', $scope.loginData);

      // Simulate a login delay. Remove this and replace with your login
      // code if using a login system
      $timeout(function() {
        $scope.closeLogin();
      }, 1000);
    };
  })

  .controller('OwnerLocationStatusesCtrl', function(ownerLocServices, $scope) {
    $scope.temperatureSliderVal = "70";
    $scope.temperatureSliderOptions = {
      from: 60,
      to: 78,
      step: 1,
      dimension: "deg"
    };
    $scope.ownerLocationStatuses = [
      { name: 'Home', status: 'HOME', id: 0, jsonId: "home" },
      { name: 'Nearby', status: 'NEARBY', id: 1, jsonId: "nearby" },
      { name: 'Away', status: 'AWAY', id: 2, jsonId: "away" }
      //{ name: 'Away for more than a day', status: 'AWAY_LONG', id: 3 }
    ];

    $scope.updateOwnerLocationStatus = function(locStatus) {
      console.log("Attemping to hit updateOwnerLocationStatus");
      console.log("Sending message that the owner is currently has a location status of: " + locStatus.name);
      ownerLocServices.postOwnerLocStatus(locStatus).then(function(response) {
        $scope.infoPosted = response.data;
      })


    };
  })

  .controller('OwnerSeasonStatusesCtrl', function(ownerSeasonServices, $scope) {
    $scope.ownerSeasonStatuses = [
      { name: 'Winter', status: 'WINTER', id: 0, jsonId: "winter"},
      { name: 'Spring', status: 'SPRING', id: 1, jsonId: "spring"},
      { name: 'Summer', status: 'SUMMER', id: 2, jsonId: "summer"},
      { name: 'Fall', status: 'FALL', id: 3, jsonId: "fall"}
    ];

    $scope.updateOwnerSeasonStatus = function(seasonStatus) {
      console.log("Attemping to hit updateOwnerSeasonStatus");
      console.log("Sending message that the owner is currently has a location status of: " + seasonStatus.name);
      ownerSeasonServices.postOwnerSeasonStatus(seasonStatus).then(function(response) {
        $scope.infoPosted = response.data;
      })
    }
  })

  .controller('TestD3LineGraphCtrl', function($scope) {
    var n = 40,
      random = d3.random.normal(0, .2),
      data = d3.range(n).map(random);
    var margin = {top: 20, right: 20, bottom: 20, left: 40},
      width = 960 - margin.left - margin.right,
      height = 500 - margin.top - margin.bottom;
    var x = d3.scale.linear()
      .domain([0, n - 1])
      .range([0, width]);
    var y = d3.scale.linear()
      .domain([-1, 1])
      .range([height, 0]);
    var line = d3.svg.line()
      .x(function(d, i) { return x(i); })
      .y(function(d, i) { return y(d); });
    var svg = d3.select("#d3graph").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    svg.append("defs").append("clipPath")
      .attr("id", "clip")
      .append("rect")
      .attr("width", width)
      .attr("height", height);
    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + y(0) + ")")
      .call(d3.svg.axis().scale(x).orient("bottom"));
    svg.append("g")
      .attr("class", "y axis")
      .call(d3.svg.axis().scale(y).orient("left"));
    var path = svg.append("g")
      .attr("clip-path", "url(#clip)")
      .append("path")
      .datum(data)
      .attr("class", "line")
      .attr("d", line);
    tick();
    function tick() {
      // push a new data point onto the back
      data.push(random());
      // redraw the line, and slide it to the left
      path
        .attr("d", line)
        .attr("transform", null)
        .transition()
        .duration(500)
        .ease("linear")
        .attr("transform", "translate(" + x(-1) + ",0)")
        .each("end", tick);
      // pop the old data point off the front
      data.shift();
    }
  })

  .controller('postOwnerStatusCtrl', ['ownerLocServices', '$scope', function(ownerLocServices, $scope) {

  }])

  .factory('ownerLocServices', function($http) {
    return {
      postOwnerLocStatus: function (ownerLocStatus) {

        return $http.post('http://192.168.2.100:1880/api/homeStatus', {
          "inHome": ownerLocStatus.jsonId
        }).then(
          function success(response) {
            console.log("posting this owner location id to json : " + ownerLocStatus.id);
            console.log(response.data);
            return response;
          },
          function error(response) {
            console.error('ERR', response);
            return response;
          });
      }
    };
  })

    .factory('ownerSeasonServices', function($http) {
      return {
        postOwnerSeasonStatus : function (ownerSeasonStatus) {
          return $http.post('http://httpbin.org/post', {
            "season" : ownerSeasonStatus.jsonId
          }).then(
            function success(response) {
              console.log("posting this owner season id to json : " + ownerSeasonStatus.id);
              console.log(response.data);
              return response;
            },
            function error(response) {
              console.error('ERR', response);
              return response;
            }
          )
        }
      };
    })
    //var locFactory = {};
    //locFactory.postOwnerLocStatus = function (ownerLoc, successCallback, errorCallback) {
    //  $http.post('http://httpbin.org/post', {
    //              'locStatusId' : ownerLoc.id
    //          }).then(function(response) {
    //    successCallback(response);
    //  }).error(function(err) {
    //    errorCallback(err);
    //  })
    //};
    //
    //return locFactory;


  .controller('OwnerSeasonStatusCtrl', function($scope, $stateParams) {

  })

  .controller('OwnerLocationStatusCtrl', function($scope, $stateParams) {
  });
