// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'angularAwesomeSlider'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
    .state('app.ownerLocationStatuses', {
      url: '/owner-location-statuses',
      views: {
        'menuContent': {
          templateUrl: 'templates/owner-location-statuses.html',
          controller: 'OwnerLocationStatusesCtrl'
        }
      }
    })

    .state('app.testD3LineGraph', {
      url: '/test-d3-line-graph',
      views: {
        'menuContent': {
          templateUrl: 'templates/test-d3-line-graph.html',
          controller: 'TestD3LineGraphCtrl'
        }
      }
    })

    .state('app.ownerSeasonStatuses', {
      url: '/owner-season-statuses',
      views: {
        'menuContent': {
          templateUrl: 'templates/owner-season-statuses.html',
          controller: 'OwnerSeasonStatusesCtrl'
        }
      }
    })

    .state('app.ownerSeasonStatus', {
      url: '/owner-season-statuses/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/owner-season-status.html',
          controller: 'OwnerSeasonStatusCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/owner-location-statuses/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'OwnerLocationStatusCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/owner-location-statuses');
});
