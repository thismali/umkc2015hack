package edu.umkc.hackathon;

/**
 * @author hastimal
 *
 * Created by hastimal jangid @ Nov 19, 2015 
 * jj.java
 */
public enum OvenSetting {

	High,
	Low
}

