package edu.umkc.hackathon;

/**
 * @author hastimal
 *
 * Created by hastimal jangid @ Nov 19, 2015 
 * KitchenController.java
 */
import java.awt.*;
import java.awt.event.*;//necessary for buttons
import java.applet.*;

public class KitchenController extends Applet implements ActionListener 
{
	//Declare a new string, and make a new "empty" mailbox
	String sMessage = new String("");

	//Declare two new buttons
	Button Button1;
	Button Button2;

	public void init() 
	{
		setBackground(Color.white);

		//Turn Layout manager off
		setLayout(null);

		//Initialize the buttons and give them names
		Button1 = new Button("TurnOn");
		Button2 = new Button("TurnOff");

		//add the buttons to the applet
		add(Button1);
		add(Button2);

		//Position Buttons(X, Y, Width, Height);
		Button1.setBounds(10,20,400,20);
		Button2.setBounds(10,60,400,20);

		//Change color of Buttons
		Button1.setBackground(Color.GREEN);
		Button2.setBackground(Color.RED);

		//Change color of text on Buttons
		Button1.setForeground(Color.BLACK);
		Button2.setForeground(Color.WHITE);

		//Make a new method that "listens" for the button press
		Button1.addActionListener(this);
		Button2.addActionListener(this);
	}

	public void actionPerformed(ActionEvent ae) 
	{
		//store the name of the button pressed in sString
		String sString = ae.getActionCommand();
		if (sString.equals("TurnOn")){
			//sMessage = "Button Number 1 was Pressed";
			StoveSensor1 sensor = new StoveSensor1();

			sensor.ovenSetting = OvenSetting.High;

			sensor.placeDish();//can be set from a UI
			try {
				sensor.turnOn();

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} }
		else if (sString.equals("TurnOff"))
			sMessage = "Button Number 2 was Pressed";
		repaint();
	}

	public void paint (Graphics g) 
	{
		g.drawString(sMessage,10,100);
	}
}
