package edu.umkc.hackathon;

/**
 * @author hastimal
 *
 * Created by hastimal jangid @ Nov 19, 2015 
 * jsh.java
 */
import java.util.Random;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class StoveSensor implements MqttCallback {

	public final static String DEFAULT_EVENT_ID = "eid";
	public final static String DEFAULT_CMD_ID = "cid";

	MqttClient client;

	// should stop sending signals when turned off
	boolean isSensorOn = true;

	int sensedWeight = 0; // this is initially zero unless dish is placed before
							// oven is turned on (no pre-heat)
	
	//overn setting (High/Low)
	OvenSetting ovenSetting;

	String deviceID="bc6a29ab629d"; //can be accepted from UI
	
	// broker
	String broker = "tcp://quickstart.messaging.internetofthings.ibmcloud.com";

	private Random random;

	public static void main(String[] args) throws InterruptedException {

		StoveSensor sensor = new StoveSensor();
		
		sensor.ovenSetting = OvenSetting.High;
		
		sensor.placeDish();//can be set from a UI
		sensor.turnOn(); //can be set form a UI
		
	}

	public void placeDish()
	{
		sensedWeight = 500;// for now default to 500 grams
	}
	public void turnOn() throws InterruptedException {
		try {
			
			random = new Random();

			int min = 0, max = 0;

			if (this.ovenSetting == ovenSetting.Low) {
				min = 163;
				max = 191;
			} else if (this.ovenSetting == ovenSetting.High) {
				min = 216;
				max = 233;
			}

			int heat = random.nextInt(max - min) + max;
			String payload = String
					.format("{\"sensorType\":\"stove\", \"json_data\": { \"temperature\": %d, \"weight\": %d}}",
							heat, this.sensedWeight);
			MemoryPersistence persistence = new MemoryPersistence();

			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true);

			System.out.println("Connecting to broker: " + broker);

			client = new MqttClient(broker,
					"d:quickstart:smart-appliance:"+deviceID, persistence);
			
			
			client.setTimeToWait(10000);

			MqttMessage message = new MqttMessage(payload.getBytes());
			message.setQos(0);
			message.setPayload(payload.getBytes());

			client.connect(connOpts);
			System.out.println("Successfully connected!");
			
			//subscribe to receiving events/commands
			//not working at the moment
			 //client.subscribe("iot-2/cmd/" + DEFAULT_CMD_ID + "/fmt/json", 0);
			 
			while (isSensorOn) {

				// client.publish("iot-2/type/\"st-app\"/id/bc6a29ab629d/evt/status/fmt/json",
				// message);

				System.out.println("sending message: " + payload);
				client.publish("iot-2/evt/" + DEFAULT_EVENT_ID + "/fmt/json",
						message);
				Thread.sleep(1000);
				
				heat = random.nextInt(max - min) + max;
				payload = String
						.format("{\"sensorType\":\"stove\", \"json_data\": { \"temperature\": %d, \"weight\": %d}}",
								heat, this.sensedWeight);

			}

			client.disconnect();

		} catch (MqttException e) {
			e.printStackTrace();
		}
		
		 
	}

	public void connectionLost(Throwable error) {


		error.printStackTrace();
	}

	public void deliveryComplete(IMqttDeliveryToken token) {
		// do nothing
		
	}

	public void messageArrived(String topic, MqttMessage messageCommand) throws Exception {

		//did we receive a shutdown command for safety?
		if(messageCommand.equals("turnoff"))
		{
			this.isSensorOn = false;
		}
		
		System.out.println(messageCommand);
		
	}
}