package umkc.smarthome;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
 
import javax.print.attribute.standard.Media;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.umkc.hackathon.StoveTemperature;
import edu.umkc.hackathon.Stove;
import edu.umkc.hackathon.StoveSensor;
 
@Path("/")
public class StoveServices {
	
	public StoveSensor sensor;
	
	@POST
	@Path("/turnOn")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response turnOnSensor(Stove stove) {
		
		try{
			
			sensor = new StoveSensor();
			
			sensor.setStoveBurner(stove);
			
			
	        Thread thread = new Thread(sensor);
	        thread.start();
	        System.out.println("Successfully started the thread and turned on sensor.");
		
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
		}
		
		
		// return HTTP response 200 in case of success
		return Response.status(200).entity("\"Response\":\"Success\"").build();
	}
	
	@GET
	@Path("/turnOff")
	@Produces(MediaType.TEXT_PLAIN)
	public Response turnOff() {
		
		try{
			
//			sensor.isSensorOn = false;					
			
			// return HTTP response 200 in case of success
			return Response.status(200).entity("Successfully turned off sensor").build();
	        
		
		} catch (Exception e) {
			e.printStackTrace();			
			
		}
		
		
		// return HTTP response 200 in case of success
		return Response.status(200).entity("\"Response\":\"Error\"").build();
	}
	
	@POST
	@Path("/placeDish")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response placeDish(String weight) {
		
		try{
			
//			sensor.setSensedWeight(Integer.parseInt(weight));					
			
			// return HTTP response 200 in case of success
			return Response.status(200).entity("\"Response\":\"Success\"").build();
	        
		
		} catch (Exception e) {
			e.printStackTrace();			
			
		}
		
		
		// return HTTP response 200 in case of success
		return Response.status(200).entity("\"Response\":\"Error\"").build();
	}
	
	@POST
	@Path("/heatlevel")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response setHeatLevel(String heat) {
		
		try{
			
//			sensor.sensedHeatLevel = Integer.parseInt(heat);					
			
			// return HTTP response 200 in case of success
			return Response.status(200).entity("\"Response\":\"Success\"").build();
	        
		
		} catch (Exception e) {
			e.printStackTrace();			
			
		}
		
		
		// return HTTP response 200 in case of success
		return Response.status(200).entity("\"Response\":\"Error\"").build();
	}
 
	@GET
	@Path("/ping")
	@Produces(MediaType.TEXT_PLAIN)
	public Response ping(InputStream incomingData) {
		String result = "Service successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
 
}