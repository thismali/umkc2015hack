package edu.umkc.hackathon;

public enum StoveTemperature {
	OFF,
	SIMMER,
	LOW,
	LOW_MEDIUM,
	MEDIUM,
	MEDIUM_HIGH,
	HIGH
	
}
