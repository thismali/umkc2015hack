package edu.umkc.hackathon;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonProperty;

public class Stove implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("temperature")
	StoveTemperature temperature;
	
	@JsonProperty("sensedWeight")
	private int sensedWeight;

	public Stove(StoveTemperature temp, int weight) {
		this.sensedWeight = weight;
		this.temperature = temp;
	}
	
	public Stove(StoveTemperature temp) {
		// TODO Auto-generated constructor stub
		this.temperature = temp;
		this.sensedWeight = 0;
	}
	
	public Stove() {
		this.temperature = StoveTemperature.OFF;
		this.sensedWeight = 0;
	}

	public StoveTemperature getTemperature() {
		return temperature;
	}

	public void setTemperature(StoveTemperature temperature) {
		this.temperature = temperature;
	}

	public int getSensedWeight() {
		return sensedWeight;
	}

	public void setSensedWeight(int sensedWeight) {
		this.sensedWeight = sensedWeight;
	}
	
	
}
