package edu.umkc.hackathon;

public class Oven {
	private int temperature;
	public boolean isOn;
	
	public boolean isOn() {
		return isOn;
	}

	public void setIsOn(boolean isOn) {
		this.isOn = isOn;
	}
	
	public boolean shouldBeOn;
	
	public boolean isShouldBeOn() {
		return shouldBeOn;
	}

	public void setShouldBeOn(boolean shouldBeOn) {
		this.shouldBeOn = shouldBeOn;
	}
	
	public Oven() {
		this.setTemperature(0);
		isOn = false;
		shouldBeOn = false;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		if (isOn && temperature == 0) {
			this.isOn = false;
		} else {
			this.temperature = temperature;
		}
	}
	
	
	public boolean needsTurnOff() {
		return (isOn && !shouldBeOn);
	}
}
